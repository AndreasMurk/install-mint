#!/usr/bin/env bash

DOT_DIR="$DEV/dot-files"

# install packages
sudo apt-get install tmux -y

cd "$HOME"

# clone repository
if [[ ! -d ".tmux" ]]; then
  git clone https://github.com/gpakosz/.tmux.git
  ln -s -f ".tmux/.tmux.conf"
fi

# source files
tmux 
tmux source-file "$HOME/.tmux.conf.local"
