#!/usr/bin/env bash

PKG_NAME="jetbrains-toolbox-1.16.6319"

# grab latest toolbox
wget https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.16.6319.tar.gz

# extract archive
tar -xzvf "$PKG_NAME".tar.gz

# change to directory and run install script
cd "$PKG_NAME"

# install jetbrains toolbox
./jetbrains-toolbox

# change to current base directory
cd ..

# clean archive and install file
rm -rf "$PKG_NAME".tar.gz 
rm -rf "$PKG_NAME"
