#!/usr/bin/env bash

# get current stable release from docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# apply permissions to binary file
sudo chmod +x /usr/local/bin/docker-compose

# (optional) set symbolic link 
#sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
