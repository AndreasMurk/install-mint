#!/usr/bin/env bash

packages=(
	git-extras
	ack
	bat
	gnome-shell-pomodoro
	exuberant-ctags
	tree
	xdotool
	zathura
	partitionmanager
	build-essential
	nextcloud-desktop
	zathura
	tmuxinator
	keepassxc
	tigervnc-viewer
	wireguard
	openvpn
	python3-pip
	python3.8
	stow
	octave
	xclip
	network-manager-vpnc-gnome
	silversearcher-ag
	gnome-clocks
	curl
)

for package in "${packages[@]}"; do
  figlet "$package"
  sudo apt-get -qq install "$package" -y > /dev/null
done
