#!/usr/bin/env bash

# add required tex packages
sudo apt-get install \
	pandoc \
	texlive-latex-extra \
	texlive \
	texlive-science \
	texlive-lang-german \
	texlive-bibtex-extra \
	latexmk -y

