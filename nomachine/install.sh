#!/usr/bin/env bash

PKG_NAME="nomachine_6.9.2_1_amd64.deb"

# get binary file
wget https://download.nomachine.com/download/6.9/Linux/"$PKG_NAME"

# install packages
sudo dpkg -i "$PKG_NAME"

# remove installation file
rm -rf "$PKG_NAME"

