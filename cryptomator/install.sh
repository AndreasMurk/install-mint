#!/usr/bin/env bash

# add repository
sudo add-apt-repository ppa:sebastian-stenzel/cryptomator -y

# install packages
sudo apt-get update && sudo apt-get install cryptomator
