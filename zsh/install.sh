#!/usr/bin/env bash

# install packages
sudo apt-get install zsh -y

# change default shell to zsh
# TODO: it asks for password
chsh --shell $(which zsh)

# install oh-my-zsh to zsh directory
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" &
