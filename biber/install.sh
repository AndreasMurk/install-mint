#!/usr/bin/env bash

# get latest download
wget "https://sourceforge.net/projects/biblatex-biber/files/latest/download" -P "$HOME/Downloads/"

# untar archive
tar -xvzf "download.tar.gz"

# move package for global usage
sudo mv "$PKG_NAME/download" /usr/local/bin/biber

