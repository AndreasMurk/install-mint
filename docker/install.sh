#!/usr/bin/env bash

# uninstall old versions
sudo apt-get remove docker docker-engine docker.io containerd runc

# install required packages
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

# add docker gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# add stable repository
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   focal \
   stable"

# update cache
sudo apt-get update

# install docker 
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

# post installation steps
sudo groupadd docker &

sudo usermod -aG docker $USER &

newgrp docker & 
