#!/usr/bin/env bash

# grab LOOP ssh-cert-authority
sudo curl https://gitlab.agentur-loop.com/loop-public/ssh-cert-authority/raw/master/ssh-cert-authority -o "$SSH_DIR/ssh-cert-authority"

# install ssh-cert-authority
sudo install "$SSH_DIR/ssh-cert-authority" /usr/local/bin

