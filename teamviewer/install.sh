#!/usr/bin/env bash

# get installer file
wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb

# update cache
sudo apt-get update

# install packages
sudo dpkg -i ./teamviewer_amd64.deb 

# fix broken packages
sudo apt-get install -fy

# clean installer file
rm -rf teamviewer_amd64.deb
