#!/usr/bin/env bash

FONT_DIR="/usr/share/fonts/opentype"

# install powerline symbols
sudo apt-get install fonts-powerline -y

# create opentype font directory if needed
if [[ ! -d $FONT_DIR ]]; then
	sudo mkdir $FONT_DIR 
fi

# get droid sans mono nerd font
if [[ ! -f "$FONT_DIR/droidsansmono.otf" ]]; then
	sudo wget \
		"https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf" \
		-O "$FONT_DIR/droidsansmono.otf"
fi

# get fira code nerd font
if [[ ! -f "$FONT_DIR/firacode.ttf" ]]; then
	sudo wget "https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/FiraCode/Regular/complete/Fira%20Code%20Regular%20Nerd%20Font%20Complete.ttf" \
		-O "$FONT_DIR/firacode.ttf"
fi

# update font cache
sudo fc-cache -f -v

plugins=(
	zsh-completions
	zsh-autosuggestions
)

for plugin in "${plugins[@]}"; do 
	figlet "$plugin"
	if [[ ! -d "$ZSH_CUSTOM/plugins/$plugin" ]]; then
		git clone https://github.com/zsh-users/"$plugin" ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/"$plugin"
	fi
done

# warhol installation
if [[ ! -d "$ZSH_CUSTOM/warhol" ]]; then
	figlet warhol
	git clone https://github.com/unixorn/warhol.plugin.zsh.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/warhol
fi

# powerlevel9k installation
if [[ ! -d "$ZSH_CUSTOM/themes/powerlevel9k" ]]; then
	figlet powerlevel9k
	git clone https://github.com/bhilburn/powerlevel9k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel9k
fi

# fzf installation
if [[ ! -d "$HOME/.fzf" ]]; then
	figlet fzf
	git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
	sudo bash ~/.fzf/install
fi

# remove old .zshrc 
sudo rm -rf "$HOME/.zshrc"

# change permissions of . directories
sudo chown -R $USER:$USER "$HOME/.cache"
sudo chown -R $USER:$USER "$HOME/.oh-my-zsh"

# change to private dev dir
if [[ ! -d "$HOME/Developing/private" ]]; then
	sudo mkdir -p "$HOME/Developing/private"
fi

cd "$HOME/Developing/private"

# grab dot-files repository
if [[ ! -d 'dot-files' ]]; then
	figlet dot-files
	git clone git@gitlab.com:AndiM202/dot-files.git
fi

# delete default .profile
rm -rf "$HOME/.profile"

cd "dot-files"

# symlink all files with stow
stow -t ~ $(ls -d */) -v

# load terminal profile
dconf load /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/ < "$HOME/.config/dark_theme.dconf"

# source .zshrc
source "$HOME/.zshrc"
