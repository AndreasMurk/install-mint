#!/usr/bin/env bash

command -v signal-desktop > /dev/null || {

	# Retrieve key and add it
	curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -

	# Add repository to signal-xenial.list 
	echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list

	# install packages
	sudo apt update && sudo apt install signal-desktop -y

}
