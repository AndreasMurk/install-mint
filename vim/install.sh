#!/usr/bin/env bash

# install vim packages
sudo apt-get install vim -y

# install neovim packages
sudo apt-get install neovim -y


