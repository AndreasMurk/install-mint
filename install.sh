#!/usr/bin/env bash

# add user to sudoers file
echo "$USER ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers >/dev/null

# create required directories
[ ! -d "$HOME/Developing" ] && mkdir -p "$HOME/Developing"

[ ! -d "$HOME/Developing/private" ] && mkdir -p "$HOME/Developing/private"

# update system
sudo apt-get update 
sudo apt-get upgrade -y 
sudo apt-get dist-upgrade -y 

# install figlet for ascii banners
sudo apt-get install -qq -y figlet >/dev/null 2>&1

packages=(
	zsh
	misc-packages
	vim
	docker
	docker-compose
	snap
	git
	jetbrains
	nomachine
	cryptomator
	nodejs
	terminal
	tmux
	signal
	teamviewer
	postman
	composer
	xampp
	loop-ssh-cert-authority
	xournalpp	
	base16
)

for package in "${packages[@]}"; do
	# ascii banner for each package
	figlet "$package"
	bash "$package"/install.sh 
done

# post installations steps

# change user of directories
#for dir in "$HOME"; do
#	if [[ ! $(stat -c '%U') == $USER ]]; then
#		# change owner to current user
#		chown -R $USER:$USER $dir
#	fi
#done
