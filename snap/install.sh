#!/usr/bin/env bash

# remove file to ensure snap installation 
sudo rm /etc/apt/preferences.d/nosnap.pref
# install packages
sudo apt-get install snapd -y

packages=(
    discord
    spotify
    vscode
    chromium-browser
    slack
    ccls
    dbeaver
    )

for package in "${packages[@]}"; do
  figlet "$package"
  sudo bash "snap/$package"/install.sh
done

