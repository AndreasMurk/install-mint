#!/usr/bin/env bash

APP_DIR="$HOME/.local/share/applications"
POSTMAN_ICON="postman.desktop"

# get installer file
wget https://dl.pstmn.io/download/latest/linux64 -O postman-linux-x64.tar.gz

# unzip package
sudo tar -xvzf postman-linux-x64.tar.gz -C /opt

# create symbolic link
sudo ln -s /opt/Postman/Postman /usr/local/bin/postman

# create directory if not there
if [[ ! -d $APP_DIR ]]; then
  mkdir $APP_DIR
fi

# create file
touch "$APP_DIR/$POSTMAN_ICON"

# create desktop icon
cat << EOF > "$APP_DIR/$POSTMAN_ICON"
[Desktop Entry]
Name=Postman
GenericName=API Client
X-GNOME-FullName=Postman API Client
Comment=Make and view REST API calls and responses
Keywords=api;
Exec=/opt/Postman/Postman
Terminal=false
Type=Application
Icon=/opt/Postman/app/resources/app/assets/icon.png
Categories=Development;Utilities;
EOF

# clean installer file
rm -rf postman-linux-x64.tar.gz
