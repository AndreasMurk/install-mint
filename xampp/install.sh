#!/usr/bin/env bash

VERSION="7.3.22"
XAMPP="xampp-linux-x64-$VERSION-0-installer.run"
DOWNLOAD_DIR="$HOME/Downloads"

# get installer file
wget https://www.apachefriends.org/xampp-files/$VERSION/xampp-linux-x64-$VERSION-0-installer.run -P "$DOWNLOAD_DIR"

# install packages
sudo chmod +x "$DOWNLOAD_DIR/$XAMPP"

cd "$DOWNLOAD_DIR"

# run installer
sudo ./"$XAMPP" &

# remove installer
sudo rm -rf "$XAMPP"
