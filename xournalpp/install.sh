#!/usr/bin/env bash

# add PPA
sudo add-apt-repository ppa:apandada1/xournalpp-stable -y

# update cache
sudo apt update

# install package
sudo apt install xournalpp -y 
