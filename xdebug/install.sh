#!/usr/bin/env bash

VERSION="xdebug-2.9.6"
LIB_VERSION="20180731"
# TODO: make this generic with different php versions (7.3.20 or 7.4.8)
LIB_DIR="/opt/lampp/lib/php/extensions/no-debug-non-zts-$LIB_VERSION"
PHP_INI="/opt/lampp/etc/php.ini"

# grab xdebug
wget "http://xdebug.org/files/$VERSION.tgz"

# install pre-requisites
sudo apt-get install -qq -y php-dev autoconf automake

# untar file
tar -xvzf "$VERSION.tgz"

# change directory
cd "$VERSION"

# run phpize
sudo phpize

# configure
./configure

# make
sudo make

# copy modules
sudo cp modules/xdebug.so "$LIB_DIR"

# define xdebug section as heredoc
read -r -d '' HEREDOC <<'EOF'
[XDebug]
zend_extension = /opt/lampp/lib/php/extensions/no-debug-non-zts-20180731/xdebug.so
xdebug.idekey=PHPSTORM
xdebug.remote_enable=1
xdebug.remote_port=9000
EOF


# insert xdebug parameters to php.ini
echo "$HEREDOC" | sudo tee -a "$PHP_INI" > /dev/null

# include vhosts file
sudo sed -i 's|#Include\ etc/extra/httpd-vhosts.conf|Include\ etc/extra/httpd-vhosts.conf|g' /opt/lampp/etc/httpd.conf

# disable track errors in php
sudo sed -i 's/track_errors=On/track_errors=Off/g' /opt/lampp/etc/php.ini 

# go to base directory 
cd "../"

# remove arbitrary files
sudo rm -rf $VERSION $VERSION.tgz package.xml
