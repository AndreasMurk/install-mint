#!/usr/bin/env bash

# Get latest wp-cli
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar

# Move it for global usage
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp
