#!/usr/bin/env bash

# install base16-shell
[[ ! -d "$HOME/.config/base16-shell" ]] && {
	git clone https://github.com/chriskempson/base16-shell.git ~/.config/base16-shell
}
